﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using static KeepRunning.Properties.Settings;

namespace KeepRunning
{
    public class Program
    {
        public static Boolean running = true;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MyCustomApplicationContext());
            LoadFiles();
        }

        
        public static string[] files;

        public static void LoadFiles()
        {
            files = new DirectoryInfo(Default.FolderPath).GetFiles().Select(f => f.FullName).ToArray();
            if (Default.EnabledList.Length < files.Length)
            {
                Default.EnabledList = Default.EnabledList.PadRight(files.Length, '1');
            }
            else if (Default.EnabledList.Length > files.Length)
            {
                Default.EnabledList = Default.EnabledList.Substring(0, files.Length);
            }
        }
    }

    public class MyCustomApplicationContext : ApplicationContext
    {
        private NotifyIcon trayIcon;

        public MyCustomApplicationContext()
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(Exit);
            // Initialize Tray Icon
            trayIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.KeepRunning,
                ContextMenu = new ContextMenu(new MenuItem[] {
                new MenuItem("Settings", ShowSettings),
                new MenuItem("Exit", Exit)
            }),
                Visible = true
            };
        }

        void ShowSettings(object sender, EventArgs e)
        {
            new Edit().ShowDialog();
        }

        void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            trayIcon.Visible = false;
            Program.running = false;
            Application.Exit();
        }
    }
}
