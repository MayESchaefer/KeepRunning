﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using static KeepRunning.Properties.Settings;

namespace KeepRunning
{
    public partial class Edit : Form
    {
        public Edit()
        {
            InitializeComponent();
        }

        private void Edit_Load(object sender, EventArgs e)
        {
            
            Program.files = new DirectoryInfo(Default.FolderPath).GetFiles().Select(f => f.FullName).ToArray();
            if (Default.EnabledList.Length < Program.files.Length)
            {
                Default.EnabledList = Default.EnabledList.PadRight(Program.files.Length, '1');
            }
            else if (Default.EnabledList.Length > Program.files.Length)
            {
                Default.EnabledList = Default.EnabledList.Substring(0, Program.files.Length);
            }
            blankLabel.Select();
            CheckBox[] checkBoxes = new CheckBox[Program.files.Length];
            for (int i = 0; i < checkBoxes.Length; i++)
            {
                checkBoxes[i] = new CheckBox()
                {
                    Checked = Default.EnabledList[i] == '1',
                    Text = Path.GetFileNameWithoutExtension((new FileInfo(Program.files[i])).Name),
                    Location = new Point(12, 41 + 23 * i),
                    AutoSize = true,
                    Tag = i
                };
                checkBoxes[i].CheckedChanged += new EventHandler(checkBoxes_checkedChanged);
                Controls.Add(checkBoxes[i]);
            }
            Height = 84 + 23 * Program.files.Length;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog f = new FolderBrowserDialog())
            {
                f.SelectedPath = Default.FolderPath;
                DialogResult result = f.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(f.SelectedPath) && Directory.Exists(f.SelectedPath))
                {
                    Default.FolderPath = f.SelectedPath;
                }
            }
            for (int i = 0; i < Program.files.Length; i++)
            {
                Controls.RemoveAt(2);
            }
            Edit_Load(new object(), new EventArgs());
        }

        private void checkBoxes_checkedChanged (object sender, EventArgs e)
        {
            StringBuilder builder = new StringBuilder(Default.EnabledList);
            builder[(int)((CheckBox)sender).Tag] = ((CheckBox)sender).Checked ? '1' : '0';
            Default.EnabledList = builder.ToString();
        }

        private void Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            Default.Save();
        }
    }
}
