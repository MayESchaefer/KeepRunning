# KeepRunning
A simple C# program to execute a program whenever it's not detected as running.
It accepts a single commandline argument, which is the name of the program to keep running.
If no argument is provided, it automatically searches for the file "hcyet.exe", which is an old Ludum Dare game which closed itself every time you lost.
