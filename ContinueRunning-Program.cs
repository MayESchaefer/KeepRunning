﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ContinueRunning
{
    class Program
    {
        static string FileName = "HCYET";
        static bool ContinueRunningProcess = true;
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                FileName = args[0];
            }
            new System.Threading.Thread(KeepRunning).Start();
            Console.ReadKey(true);
            ContinueRunningProcess = false;
        }

        static void KeepRunning ()
        {
            while (ContinueRunningProcess)
            if (!IsProcessOpen(FileName))
            {
                Process.Start(FileName);
                    SetForegroundWindow(GetProcessHwnd(FileName));
            }
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        static bool IsProcessOpen (string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }

            return false;
        }

        static IntPtr GetProcessHwnd (string name)
        {
            IntPtr hWnd = IntPtr.Zero;
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.Contains(name))
                {
                    hWnd = pList.MainWindowHandle;
                }
            }
            return hWnd;
        }
    }
}
